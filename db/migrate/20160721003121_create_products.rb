class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name
      t.string :sku
      t.text :description
      t.text :cover
      t.string :category
      t.integer :status

      t.timestamps
    end
  end
end
