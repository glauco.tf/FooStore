class CreateNewsProducts < ActiveRecord::Migration
  def change

    # product 1
    I18n.locale = "pt-br"
    p = Product.new
    p.name = "Project Cars"
    p.description = "Project CARS (Community Assisted Racing Simulator) é um jogo do género simulador de corrida desenvolvido pela companhia Slightly Mad Studios. Foi lançado para PlayStation 4, Xbox One e Microsoft Windows em 7 de maio de 2015, e para Wii U e SteamOS mais tarde em 2015.[1] Project CARS seria originalmente desenvolvido para Microsoft Windows, Wii U, PlayStation 3 e Xbox 360, mas a 6 de Novembro de 2013, a Slightly Mad Studios anunciou que as versões para PlayStation 3 e Xbox 360 seriam canceladas.[2] O jogo passou também por uma série de atrasos, e passou o seu lançamento de novembro de 2014 para maio de 2015.
                    O projeto foi financiado pela comunidade e pelos próprios desenvolvedores, sem o tradicional incentivo de um distribuidor. Através da compra de pacotes, jogadores puderam contribuir para o desenvolvimento em diferentes partes do projeto.[3] Os membros têm alguns benefícios de acordo com o pacote que adquiriram, recebendo uma parte do lucro das vendas, gerados nos primeiros 2 anos após o lançamento, como recompensa pelo seu esforço, a ser pago trimestralmente.[4] Os membros têm também acesso ao fórum privado da Slightly Mad Studios, o World of Mass Development."
    p.cover = "http://www.gamestorrent.biz/wp-content/uploads/2015/02/project_cars_cover_by_rafamb91-d7piiwx.jpg"
    p.category = 5
    p.status = 1
    p.save

    I18n.locale = "en"
    p.sku = "NU-" + Time.now.strftime("%Y%m") + "-" + p.id.to_s.rjust(5, '0')
    p.description = "Project CARS (Community Assisted Racing Simulator) is a game genre racing simulator developed by the company Slightly Mad Studios. It was released for PlayStation 4, Xbox One and Microsoft Windows on May 7, 2015, and Wii U and steamos later in 2015. [1] Project CARS was originally developed for Microsoft Windows, Wii U, PlayStation 3 and Xbox 360, but on 6 November 2013, the Slightly Mad Studios announced that versions for PlayStation 3 and Xbox 360 would be canceled. [2] the game also underwent a series of delays, and passed its launch in November 2014 to May 2015.
                    The project was funded by the community and developers themselves, without the traditional incentive distributor. By purchasing packets, players could contribute to the development in different parts of the project. [3] The members have some benefits according to the package purchased, receiving a share of the sales generated in the first 2 years after the launch as a reward for their efforts, to be paid quarterly. [4] members also have access to the private forum of Slightly Mad Studios, the World of Mass Development."
    p.save


    # product 2
    I18n.locale = "pt-br"
    p = Product.new
    p.name = "Battlefield"
    p.description = "Participe de batalhas mais significativas com Operações, um novo jeito de jogar o modo multiplayer de Battlefield. Batalhe como agressor ou defensor em uma série de conflitos interconectados espalhados entre diversos mapas. Viva as origens das guerras modernas, quando o antigo mundo foi destruído e deu lugar ao novo. Use as armas e veículos inovadores e modernos da I Guerra Mundial em batalhas em terra, céu e mar. Pilote os maiores veículos da história do Battlefield. Faça chover fogo do céu em um dirigível gigantesco, rasgue o mundo em um trem blindado, ou bombardeie a terra a partir do mar no encouraçado."
    p.cover = "http://gamepreorders.com/wp-content/uploads/2016/05/battlefield-1-cover-art.jpg"
    p.category = 3
    p.status = 1
    p.save

    I18n.locale = "en"
    p.sku = "NU-" + Time.now.strftime("%Y%m") + "-" + p.id.to_s.rjust(5, '0')
    p.description = "Join more significant battles Operations, a new way of playing the multiplayer of Battlefield. Battle as attacker or defender in a series of interconnected conflicts scattered among different maps. Long live the origins of modern warfare, when the old world was destroyed and gave way to the new. Use the weapons and innovative vehicles and modern of World War I in battles on land, sky and sea. Pilote larger vehicles in the history of Battlefield. Make it rain fire from the sky in a giant airship, rip the world in an armored train, or bombard the earth from the sea in the battleship."
    p.save

  end
end
