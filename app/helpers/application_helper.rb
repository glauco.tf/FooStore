module ApplicationHelper
  def productCategory
    [
      ['Ação', 1],
      ['Aventura', 2],
      ['Tiro', 3],
      ['Terror', 4],
      ['Corrida', 5],
      ['Rpg', 6],
      ['Arcade', 7]
    ]
  end

  def productStatus
    [
      ['Não Publicado', 0],
      ['Publicado', 1]
    ]
  end

  def getProductCategory(id_category)
    productCategory[id_category.to_i - 1][0]
  end

  def getProductStatus(id_status)
    productStatus[id_status.to_i][0]
  end
end
