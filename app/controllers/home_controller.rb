class HomeController < ApplicationController
  layout "site"

  def index
    if params[:word] && params[:category]
      @products = Product.where(status: 1)
                          .where("name LIKE ? OR description LIKE ?", "%#{params[:word]}%", "%#{params[:word]}%")
                          .where("category = ?", params[:category])
    elsif params[:word] && !params[:category]
      @products = Product.where(status: 1)
                          .where("name LIKE ? OR description LIKE ?", "%#{params[:word]}%", "%#{params[:word]}%")
    elsif !params[:word] && params[:category]
      @products = Product.where(status: 1)
                          .where("category = ?", params[:category])
    else
      @products = Product.where(status: 1)
    end
  end

  def show
    if params.include?(:id)
      @product = Product.where(id: params[:id])
    end
  end

  def change_language
    if params[:language]
      cookies[:language] = params[:language]
    end
    redirect_to :back
  end

end
