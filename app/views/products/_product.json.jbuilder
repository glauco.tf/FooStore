json.extract! product, :id, :name, :sku, :description, :category, :status, :created_at, :updated_at
json.url product_url(product, format: :json)